[Introduction, spoken]
Alright guys, *cough* I'm doing 'Take On Me' now. Alright
I- I really- I really like this song, it sounds good

[Verse 1]
Mining away
I don't know what to mine
I'll mine this anyway
In this Minecraft day
So beautiful
Mine further down
What's that I found?

[Chorus]
Mine diamonds (Take on me)
Mine diamonds (Take on me)
I'll mine them
So far I've got two!

[Verse 2]
So easy to mine
With my Minecraft pickaxe and shovels
Hopefully they stay
In my Minecraft chests
So I'm gonna make
A lock on it

[Chorus]
Mine diamonds (Take on me)
Mine diamonds (Take on me)
I'll mine them
So far I've got two!

[Spoken]
*Multiple coughs*
I'm alright, not ready

[Verse 3]
All these diamonds
Sitting carefully lay
I'm getting worried
If they might get stolen from my ender chest
Wait who is that?
Holy sheep it's Notch!

[Chorus]
Mine diamonds (Take on me)
Mine diamonds (Take on me)
Now they're safe
*inaudible screaming*
Now...
Now that they're safe
*inaudible screaming*
Mine diamonds (Take on me)
Mine diamonds (Take on me)

[Ending, spoken]
Uh, Than- Thanks for listening guys and thanks for recommending this song